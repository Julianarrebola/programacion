package com.izo.veterinaria.repository;

import com.izo.veterinaria.model.Turno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Optional;

public interface TurnoRepository extends JpaRepository <Turno, Long> {


}
