package com.izo.veterinaria.repository;

import com.izo.veterinaria.model.Veterinario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Optional;

public interface VeterinarioRepository extends JpaRepository <Veterinario, Long> {
    @Query("SELECT o FROM Veterinario o WHERE o.matricula = ?1")
    Optional<Veterinario> buscar(Integer matricula);

    @Transactional
    @Modifying
    @Query("DELETE FROM Veterinario o WHERE o.matricula = ?1")
    void eliminar(Integer matricula);
}