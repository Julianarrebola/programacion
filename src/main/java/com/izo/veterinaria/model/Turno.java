package com.izo.veterinaria.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table

public class Turno {
    @Id
    private Long id;
    @SequenceGenerator(name = "turno_sequence", sequenceName = "turno_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "turno_sequence")
    private LocalDate date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private LocalTime time;
    private String veterinario;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public String getVeterinario() {
        return veterinario;
    }

    public Turno(Long id, LocalDate date, LocalTime time, String veterinario, String perro) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.veterinario = veterinario;
        this.perro = perro;
    }

    public void setVeterinario(String veterinario) {
        this.veterinario = veterinario;
    }

    public String getPerro() {
        return perro;
    }

    public void setPerro(String perro) {
        this.perro = perro;
    }

    private String perro;

    @Override
    public String toString() {
        return "Turno{" +
                "id=" + id +
                ", date=" + date +
                ", time=" + time +
                ", veterinario='" + veterinario + '\'' +
                ", perro='" + perro + '\'' +
                '}';
    }
}
