package com.izo.veterinaria.model;

import javax.persistence.*;

@Entity
@Table
public class Veterinario {
    @Id
    @SequenceGenerator(name = "odontologo_sequence", sequenceName = "odontologo_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "odontologo_sequence")
    private Long id;
    private String nombre;
    private String apellido;
    private Integer matricula;

    // Relaciones
    //@OneToMany(mappedBy = "odontologo", fetch = FetchType.LAZY)
    //private Set<Turno> turnos = new HashSet<>();

    // Constructores
    public Veterinario() {
    }

    public Veterinario(String nombre, String apellido, Integer matricula) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.matricula = matricula;
    }

    public void setId(Long id) {
        this.id = id;
    }

    // Getters y Setters
    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getMatricula() {
        return matricula;
    }

    public void setMatricula(Integer matricula) {
        this.matricula = matricula;
    }

    public Veterinario setear(Veterinario veterinario) {
        this.nombre = veterinario.getNombre();
        this.apellido = veterinario.getApellido();
        this.matricula = veterinario.getMatricula();
        return veterinario;
    }

    // ToString
    @Override
    public String toString() {
        return "Odontologo{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", matricula=" + matricula +
                '}';
    }
}