package com.izo.veterinaria.service;


import com.izo.veterinaria.model.Veterinario;
import com.izo.veterinaria.repository.VeterinarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VeterinarioService {
    private final VeterinarioRepository veterinarioRepository;

    @Autowired
    public VeterinarioService(VeterinarioRepository veterinarioRepository) {
        this.veterinarioRepository = veterinarioRepository;
    }

    // Métodos

    /* Buscar todos los Veterinario */
    public List<Veterinario> listar() {
        return veterinarioRepository.findAll();
    }

    /* Buscar Veterinario por matricula */
    public Veterinario buscar(Integer matricula) {
        return veterinarioRepository.buscar(matricula).get();
    }

    /* Guardar un nuevo Veterinario */
    public Veterinario guardar(Veterinario veterinario) {
        if (veterinario != null) {
            return veterinarioRepository.save(veterinario);
        }
        return new Veterinario();
    }

    /* Eliminar un Veterinario */
    public String eliminar(Integer matricula)  {
        String respuesta = "El odontologo de matrícula " + matricula + " no existe.";
        if(veterinarioRepository.buscar(matricula).isPresent()) {
            veterinarioRepository.eliminar(matricula);
            respuesta = "El odontologo de matrícula " + matricula + " ha sido eliminado.";
        }
        return respuesta;
    }

    /* Actualizar un Veterinario */
    public Veterinario actualizar(Veterinario veterinario) {
        return veterinarioRepository.save(veterinario);
    }
}
